#!/bin/bash
# Scroll through a file's git history (also works for directories)
# by Wolfram Rösler 2021-09-14

# Check command line
if [ $# != 1 ];then
    echo "Scroll through a file's git history"
    echo "Usage: $0 filename"
    exit 1
fi
FILE="$1"

# Terminal controls
BLD=$(tput bold)
GRY=$(tput setaf 7)
REV=$(tput rev)
OFF=$(tput sgr0)

# Load the commit IDs in the file's history
IDS=($(git log -- "$FILE" | grep "^commit " | cut -d" " -f2))
COUNT=${#IDS[@]}
if ((COUNT==0)); then
    echo "No history for $FILE"
    exit 1
fi

# Make sure we've got a clean working tree
STATUS=$(git status -sb --porcelain 2>/dev/null) || exit
if [[ $STATUS =~ [[:cntrl:]][^#] ]];then
    echo "Working tree isn't clean, please commit or stash the changes"
    exit 1
fi

# Go back to where we came from when finished
HEAD=$(git rev-parse --abbrev-ref HEAD 2>/dev/null) || exit
trap "git checkout --quiet $HEAD" EXIT

# Input loop
IDX=0
while true;do

    # Go to the current commit
    git checkout --quiet ${IDS[$IDX]}

    # Show where we are now
    git log -n1

    # Make the prompt
    PROMPT="($((COUNT-IDX))/$COUNT) e edit, l log"
    if ((IDX > 0));then
        PROMPT+=", + next, \$ youngest"
    fi
    if ((IDX < COUNT-1));then
        PROMPT+=", d diff, - previous, 0 oldest"
    fi
    PROMPT+=", q quit"

    # Read and process a command
    printf "\n%s" "$REV$GRY$PROMPT$OFF > "
    read || break
    case $REPLY in
        +)  ((--IDX));;
        -)  ((++IDX));;
        \$) IDX=0;;
        d)  git diff ${IDS[$((IDX+1))]}..${IDS[$IDX]} -- "$FILE";;
        e)  ${EDITOR:-vi} "$FILE";;
        l)  git log "$FILE";;
        q)  break;;
        [0-9]|[0-9][0-9]|[0-9][0-9][0-9]|[0-9][0-9][0-9][0-9]|[0-9][0-9][0-9][0-9][0-9]) IDX=$((COUNT-REPLY));;
        *)  echo "?";;
    esac

    # Normalise the index
    IDX=$((IDX < 0 ? 0 : IDX>=COUNT ? COUNT-1 : IDX))
done
